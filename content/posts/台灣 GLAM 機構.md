Title: 台灣 GLAM 機構
Date: 2020-08-10 21:20
Tags: GLAM, 動物園, 博物館, 圖書館, 檔案館, 畫廊
Author: Monica Mu

## 前情提要
![](https://upload.wikimedia.org/wikipedia/commons/6/61/FindingGLAMs_map_of_new_and_existing_GLAMs%2C_March_2020.png)
  
  瑞典維基分會與維基媒體基金會合作推出了 "Finding GLAM" Wikidata 專案，旨在收錄全球各地的畫廊、圖書館、檔案館、博物館 (GLAM) 的資訊進入到維基媒體計畫中。
  上圖為目前已經收錄在 Wikidata 中的並有提供 GPS 資訊的 GLAM 機構，藍色標示為 2019 年 6 月以前已經存在於 Wikidata 中的資訊，黃色則是 2019 年 6 月至 2020 年 2 月所登錄的資訊。
  
* 詳細資料可參閱 [Finding GLAM](https://meta.wikimedia.org/wiki/FindingGLAMs)


## 中華民國境內館聯機構分佈圖
館聯（GLAM）是畫廊、圖書館、檔案館、博物館四種館的簡稱，其中也包含動物園、文化中心等各式機構。

本圖所顯示之館聯分佈為 Wikidata 中所收錄的項目，同時擁有地理座標的內容，若欲查詢完整的台灣館聯機構列表，可至 [Wikidata Query 服務](https://w.wiki/ZDz) 查詢。

### 分佈圖
<iframe width="100%" height="600" src="https://demo.depositar.io/dataset/glam/resource/3096dc1f-dd22-469d-8a04-231980bf7b49/view/dd67a3aa-c450-476b-aec1-5446e809cf6f" frameBorder="0"></iframe>

### GLAM 資料表
<iframe width="100%" height="600" src="https://demo.depositar.io/dataset/glam/resource/7c0bb957-e215-40cf-ba31-5c5b7cb53f27/view/41c7db7b-82d8-4fe3-b720-9418d75bfde2" frameBorder="0"></iframe>


## GLAM in Taiwan 館聯在台灣
![](https://i.imgur.com/dVh0w4C.png)
(上圖為 2020/4/21 台灣 GLAM 機構統計數量，我們與匈牙利並列全球第 34 名)

[臺灣館聯機構查詢](https://query.wikidata.org/#%0ASELECT%20%2a%20WHERE%20%7B%0A%20%20%3Fitem%20wdt%3AP31%2Fwdt%3AP279%2a%20wd%3AQ1030034%3B%0A%20%20%20%20%20%20%20%20wdt%3AP17%20wd%3AQ865%0A%20%20%20%20%20%20%20%20%20%0A%7D)，截至 20200510 臺灣有 632 個館聯機構登錄在 Wikidata 中。

[Wikidata Taiwan](https://www.facebook.com/WikidataTW/) 其實早在社群成立前就已經發起過專案在進行中華民國館舍資訊整理，不過當時僅僅針對政府開放資料所公開的博物館進行整理，在畫廊、圖書館與檔案館的部分，都是當時未處理的部分。

為響應這個國際計畫，同時也希望透過這個方式來讓世界看到台灣社群的力量， Wikidata Taiwan 希望能夠發起 "Finding GLAM Taiwan 台灣館聯總動員"。

在這藝文產業低迷的時節，我們接著做平常忽略掉的基礎建設，為未來的振興與加值應用建構厚實的基礎。透過 Wikidata 全球性的資料串連，只要台灣的資料聚集到一定的規模，便是一種「資料庫」中的「館聯火力展示」。
