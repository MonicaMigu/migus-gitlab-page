#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'MonicaMigu'
SITENAME = "MonicaMigu's GitLab Pages"
SITETITLE = "MonicaMigu's GitLab Pages"
SITELOGO = "https://monicamigu.github.io/image/20180526_114351.jpg"
SITEURL = ''

COPYRIGHT_NAME = "Monica Migu"
COPYRIGHT_YEAR = "2020"

PATH = 'content'
OUTPUT_PATH = 'public'

THEME = 'pelican-themes/Flex'

TIMEZONE = 'UTC'

DEFAULT_LANG = 'zh-TW'


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Mozilla', 'https://www.mozilla.org/zh-TW/'),
         ('Wikidata', 'https://www.wikidata.org/wiki/Wikidata:Main_Page'),
         ('My Personal Page', 'https://monicamigu.github.io'),
         ('張媽媽流浪動物之家', 'https://www.ntpsa.org.tw/'),
         ('Wikidata Taiwan 粉專', 'https://www.facebook.com/WikidataTW/'),)

# Social widget
SOCIAL = (('github', 'https://github.com/MonicaMigu'),
          ('linkedin', 'https://www.linkedin.com/in/monicamigu'),
          ('twitter', 'https://twitter.com/Monica_Migu'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
